const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

// Check if email exists
router.post('/email-exists', (req, res) =>{
	UserController.emailExists(req.body).then(resultfromEmailExists => res.send(resultfromEmailExists))
})

// User Registration
router.post('/', (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


//Login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

// Retrieve User Details
/* auth.verify is what we called MIDDLEWARE, 
later on we're gonna make a function, vineverify yung token if legit sya.*/
router.get('/details', auth.verify, (req,res) => {
	// = {
	// 	id: user._id,
	// 	email: user.email,
	// 	isAdmin: user.isAdmin
	// }

					 //function nagagawin din natin  
	const user = auth.decode(req.headers.authorization)
	UserController.get({ userId: user.id }).then(resultFromDetails => res.send(resultFromDetails))
})

// Enroll a user 
router.post('/enroll', auth.verify, (req, res) =>{
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	UserController.enroll(params).then(result => res.send(result))
})

module.exports = router



