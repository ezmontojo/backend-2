const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI'

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

//					 
module.exports.verify = (req, res, next) => {
				// getting the access token
	let token = req.headers.authorization;

 	console.log(token);
 				// kapag di yung token di nakadefine
 	if (typeof token !== 'undefined') {

 			// ung token pag pinadala via header 'Bearer eyjh' so tinatanggal ung bearer and space and start  sa e
 		token = token.slice(7, token. length);

 		// ippoprovide mo ung token and ung secret then 
 		return jwt.verify(token, secret, (err, data) => {
 				// next() passes the request to next callback funtion/middleware
 			return (err) ? res.send({ auth: 'failed' }) : next()
 		})
 		// sayBye = callback function 

 		// sayHello(param1, sayBye()) 

 	} else {
 		return res.send({ auth: 'failed' })
 	}
}
// bcrypt 1 = A
// adasdfqwer1234 = hello123
// a12rfdsfsdacvx = hello123

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			// {complete: true} grabs both the request
			return (err) ? null : jwt.decode(token, {complete: true}).
			payload
		})
	} else {
		return null
	}
}